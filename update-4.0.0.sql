ALTER TABLE sys_config ALTER COLUMN port DROP NOT NULL ;
ALTER TABLE sys_config ALTER COLUMN ip_address DROP NOT NULL ;
UPDATE sys_config SET hl7_protocol = 1;
UPDATE sys_config SET message_encoding = 1;
UPDATE hl7_message SET message_encoding = 1;
ALTER TABLE hl7_simulator_responder_configuration ALTER COLUMN ip_address DROP NOT NULL;
ALTER TABLE hl7_simulator_responder_configuration ALTER COLUMN listening_port DROP NOT NULL;
UPDATE hl7_simulator_responder_configuration SET hl7_protocol = 1;
UPDATE hl7_simulator_responder_configuration SET message_encoding = 1;
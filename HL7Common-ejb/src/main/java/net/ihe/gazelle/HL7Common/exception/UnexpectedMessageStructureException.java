package net.ihe.gazelle.HL7Common.exception;


import ca.uhn.hl7v2.AcknowledgmentCode;
import net.ihe.gazelle.HL7Common.utils.HL7Constants;

/**
 * <p>UnexpectedMessageType class.</p>
 *
 * @author abe
 * @version 1.0: 15/04/19
 */

public class UnexpectedMessageStructureException extends HL7v2ParsingException {

    private static final String MESSAGE_TYPE_LOCATION = "MSH^1^9^0^3";

    public UnexpectedMessageStructureException() {
        super((String) null, MESSAGE_TYPE_LOCATION);
    }


    @Override
    public String getHL7ErrorCode() {
        return HL7Constants.INTERNAL_ERROR;
    }

    @Override
    public String getErrorMessage() {
        return "Message structure is not the one expected for this event";
    }

    @Override
    public AcknowledgmentCode getAckCode() {
        return AcknowledgmentCode.AR;
    }

}

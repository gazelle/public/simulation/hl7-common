package net.ihe.gazelle.HL7Common.exception;

import net.ihe.gazelle.HL7Common.utils.HL7Constants;

/**
 * <p>MissingRequiredSegmentException class.</p>
 *
 * @author abe
 * @version 1.0: 15/04/19
 */

public class DuplicateKeyIdentifierException extends HL7v2ParsingException {


    public DuplicateKeyIdentifierException(String inMissingSegment, String errorLocation) {
        super(inMissingSegment, errorLocation);
    }

    @Override
    public String getHL7ErrorCode(){
       return HL7Constants.DUPLICATE_KEY_IDENTIFIER;
    }

    @Override
    public String getErrorMessage(){
        return "This identifier is already used: " + getElementInError();
    }

}

package net.ihe.gazelle.HL7Common.action;

import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfigurationQuery;
import net.ihe.gazelle.hl7.validator.client.MessageEncoding;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <p>HL7v2ResponderSUTConfManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("hl7v2ResponderSUTConfManager")
@Scope(ScopeType.PAGE)
public class HL7v2ResponderSUTConfManager extends
        AbstractSystemConfigurationManager<HL7V2ResponderSUTConfiguration> implements Serializable, UserAttributeCommon {

    /**
     *
     */
    private static final long serialVersionUID = -8203169928429200398L;

    @In(value = "gumUserService")
    private UserService userService;

    GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteSelectedSystemConfiguration() {
        if (selectedSystemConfiguration != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            HL7V2ResponderSUTConfiguration configToRemove = entityManager.find(
                    HL7V2ResponderSUTConfiguration.class, selectedSystemConfiguration.getId());
            try {
                entityManager.remove(configToRemove);
                entityManager.flush();
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "SUT configuration successfully deleted");
            } catch (Exception e) {
                String msg = "Could not delete selected system under test because a preference is attached to it. " +
                        "You must delete this preference via the \"SUT's assigning authorities page\"";
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg);
            }
            selectedSystemConfiguration = null;
            reset();
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No SUT selected for deletion");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected HQLCriterionsForFilter<HL7V2ResponderSUTConfiguration> getHQLCriterionsForFilter() {
        HL7V2ResponderSUTConfigurationQuery query = new HL7V2ResponderSUTConfigurationQuery();
        HQLCriterionsForFilter<HL7V2ResponderSUTConfiguration> criterionsForFilter = query.getHQLCriterionsForFilter();
        criterionsForFilter.addPath("owner", query.owner());
        criterionsForFilter.addQueryModifier(this);
        return criterionsForFilter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void copySelectedConfiguration(HL7V2ResponderSUTConfiguration inConfiguration) {
        if (inConfiguration != null) {
            selectedSystemConfiguration = new HL7V2ResponderSUTConfiguration(inConfiguration);
            editSelectedConfiguration();
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No configuration selected");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addConfiguration() {
        selectedSystemConfiguration = new HL7V2ResponderSUTConfiguration();
        displayEditPanel = true;
        displayListPanel = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Usage> getPossibleListUsages() {
        UsageQuery query = new UsageQuery();
        List<Usage> usages = query.getList();
        Collections.sort(usages);
        return usages;
    }

    /**
     * <p>getHl7Protocols.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getHl7Protocols() {
        return HL7Protocol.getListForSelectMenu();
    }

    /**
     * <p>getEncodings.</p>
     *
     * @return an array of {@link net.ihe.gazelle.hl7.validator.client.MessageEncoding} objects.
     */
    public MessageEncoding[] getEncodings() {
        return MessageEncoding.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyQuery(HQLQueryBuilder<HL7V2ResponderSUTConfiguration> hqlQueryBuilder, Map<String, Object> map) {
        HL7V2ResponderSUTConfigurationQuery query = new HL7V2ResponderSUTConfigurationQuery();
        if (!Identity.instance().isLoggedIn()) {
            hqlQueryBuilder.addRestriction(query.isPublic().eqRestriction(true));
        } else if (!Identity.instance().hasRole("admin_role")) {
            hqlQueryBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
                    HQLRestrictions.eq("ownerCompany", identity.getOrganisationKeyword())));
        }
        if (filteredUsage != null) {
            hqlQueryBuilder.addRestriction(query.listUsages().id().eqRestriction(filteredUsage.getId()));
        }
    }

    /**
     * Used in editSystemConfig_common.xhtml
     */
    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}

package net.ihe.gazelle.HL7Common.responder;


/**
 * <p>ServerLocal interface.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public interface ServerLocal {

    /**
     * This method is called at start up
     *
     * TODO instanciate the entityfactory and the handler and call <code>startServers(Actor, Transaction, AffinityDomain, Application)</code>
     */
    public void startServers();

    /**
     * <p>stopServers.</p>
     */
    public void stopServers();

    /**
     * <p>startServer.</p>
     *
     * @param serverName a {@link java.lang.String} object.
     * @return a boolean.
     */
    public boolean startServer(String serverName);

    /**
     * <p>stopServer.</p>
     *
     * @param serverName a {@link java.lang.String} object.
     * @return a boolean.
     */
    public boolean stopServer(String serverName);
}

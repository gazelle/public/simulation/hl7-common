package net.ihe.gazelle.HL7Common.exception;

/**
 * <p>HL7MessageBuilderException class.</p>
 *
 * @author abe
 * @version 1.0: 16/04/19
 */

public class HL7MessageBuilderException extends Throwable {

    public HL7MessageBuilderException(Throwable cause){
        super(cause);
    }

    public HL7MessageBuilderException(String message, Throwable cause){
        super(message, cause);
    }

    public HL7MessageBuilderException(String message){
        super(message);
    }
}

package net.ihe.gazelle.HL7Common.responder;

import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.HL7Service;
import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.HL7Common.utils.GazelleHapiContext;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.contexts.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class description : <b>AbstractServer</b>
 * This class is used to set up a server to handle HL7 messages when a simulator acts as a responder
 * <p/>
 * This class has to be extended
 *
 * @author aberge > anne-gaelle.berge@ihe-europe.net
 * @version 2.0 - 2014, April 22nd
 */
public abstract class AbstractServer<T extends IHEDefaultHandler> implements Serializable, ServerLocal {

    /**
     *
     */
    private static final long serialVersionUID = -4272294550986660425L;

    protected EntityManager entityManager;

    private static Logger log = LoggerFactory.getLogger(AbstractServer.class);
    protected T handler;
    private Map<String, HL7Service> servers;
    HapiContext context = GazelleHapiContext.instance();

    /**
     * Sets the servers and start them
     *
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public void startServers(Actor simulatedActor, Transaction transaction, Domain domain) {
        List<SimulatorResponderConfiguration> configurations = SimulatorResponderConfiguration
                .getConfigurationsFiltered(null, simulatedActor, transaction, domain, entityManager, null, HL7Protocol.MLLP);
        servers = new HashMap<String, HL7Service>();
        if (configurations != null) {
            for (SimulatorResponderConfiguration config : configurations) {
                log.info("Attempting to start server " + config.getName() + " on port " + config.getListeningPort() + " ...");
                HL7Service server = registerApplicationAndStartServer(config);
                if (server != null) {
                    servers.put(config.getName(), server);
                    log.info("OK");
                } else {
                    log.error("KO (" + config.getName() + ")");
                }
            }
        }
    }

    /**
     * This method is called at start up
     *
     * TODO instanciate the entityfactory and the handler and call <code>startServers(Actor, Transaction, AffinityDomain, Application)</code>
     */
    @Create
    public abstract void startServers();

    /**
     * <p>onDestroy.</p>
     */
    @Destroy
    @Remove
    public void onDestroy(){
        if (servers != null) {
            for (String serverName : servers.keySet()) {
                try {
                    HL7Service server = servers.get(serverName);
                    server.stop();
                    servers.remove(serverName);
                } catch (Exception e) {
                    log.error("Unable to save the new state of server " + serverName, e);
                }
            }
            log.info("servers stopped !");
        } else {
            log.info("no server to stop");
        }
    }

    /**
     * <p>stopServers.</p>
     */
    public void stopServers() {
        Lifecycle.beginCall();
        entityManager = EntityManagerService.provideEntityManager();
        if (servers != null) {
            for (String serverName : servers.keySet()) {
                SimulatorResponderConfiguration config = SimulatorResponderConfiguration
                        .findSimulatorResponderConfigurationByName(serverName, entityManager);
                try {
                    HL7Service server = servers.get(serverName);
                    server.stop();
                    servers.remove(serverName);
                    config.setRunning(false);
                    config.save(entityManager);
                } catch (Exception e) {
                    log.error("Unable to save the new state of server " + serverName, e);
                }
            }
            log.info("servers stopped !");
        } else {
            log.info("no server to stop");
        }
        try {
            if (context != null) {
                context.close();
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }


    /** {@inheritDoc} */
    public boolean stopServer(String serverName) {
        if (servers != null && servers.containsKey(serverName)) {
            log.info("Stopping server: " + serverName);
            HL7Service serverToStop = servers.get(serverName);
            if (serverToStop.isRunning()) {
                serverToStop.stop();
                servers.remove(serverName);
                return true;
            } else {
                log.info("This server is already stopped");
                return true;
            }
        } else {
            log.error("Server " + serverName + " is not in the list of servers managed by this bean: "
                    + this.getClass().getName());
            return false;
        }
    }

    /**
     * {@inheritDoc}
     *
     * start a server. If the server is not present in servers map, retrieve its configuration and start it
     */
    public boolean startServer(String serverName) {
        if (servers != null && servers.containsKey(serverName)) {
            log.info("Starting server: " + serverName);
            HL7Service serverToStart = servers.get(serverName);
            if (!serverToStart.isRunning()) {
                try {
                    serverToStart.startAndWait();
                    return true;
                } catch (RuntimeException e) {
                    log.error("Cannot start server " + serverName + " " + e.getMessage());
                    return false;
                } catch (InterruptedException e) {
                    log.error(e.getMessage(), e);
                    return false;
                }
            } else {
                log.info("This server is already started");
                return true;
            }
        } else {
            log.info("This server is not registered, register it");
            SimulatorResponderConfiguration config = SimulatorResponderConfiguration
                    .findSimulatorResponderConfigurationByName(serverName, entityManager);
            if (config != null) {
                HL7Service server = registerApplicationAndStartServer(config);
                servers.put(serverName, server);
            } else {
                log.error("No configuration found for name: " + serverName);
                return false;
            }
            return true;
        }
    }

    /**
     * The transaction must be opened when calling this method
     *
     * @param config
     */
    private HL7Service registerApplicationAndStartServer(SimulatorResponderConfiguration config) {
        // create a handler from the given one and set the appropriate charset
        IHEDefaultHandler defaultHandler = handler.newInstance();
        defaultHandler.setServerApplication(config.getReceivingApplication());
        defaultHandler.setServerFacility(config.getReceivingFacility());
        defaultHandler.setSimulatedActor(config.getSimulatedActor());
        defaultHandler.setSimulatedTransaction(config.getTransaction());
        defaultHandler.setDomain(config.getDomain());
        HL7Service server = context.newServer(config.getListeningPort(), false);
        server.registerConnectionListener(new GazelleConnectionListener());
        server.setExceptionHandler(new GazelleHL7ExceptionHandler(config));
        server.registerApplication(defaultHandler);
        // start on startup
        try {
            server.startAndWait();
            config.setRunning(true);
        } catch (RuntimeException e) {
            log.error("Cannot start server " + config.getName() + " " + e.getMessage(), e);
            config.setRunning(false);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            config.setRunning(false);
        } catch (Throwable e){
            log.error(e.getMessage(), e);
            config.setRunning(false);
        }
        try {
            config = config.save(entityManager);
        } catch (Exception e) {
            log.error("Unable to save the new state of server : " + config.getName(), e);
        }
        if (config.getRunning() != null && config.getRunning()) {
            return server;
        }else {
            return null;
        }
    }

}

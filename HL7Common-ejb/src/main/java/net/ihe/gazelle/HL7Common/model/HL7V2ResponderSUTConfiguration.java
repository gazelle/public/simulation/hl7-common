package net.ihe.gazelle.HL7Common.model;

import net.ihe.gazelle.hl7.validator.client.MessageEncoding;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Identity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * <b>Class Description : </b>HL7V2ResponderSUTConfiguration<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 07/09/15
 */

@Entity
@Name("hl7v2ResponderSUTConfiguration")
@DiscriminatorValue("hl7v2_responder")
public class HL7V2ResponderSUTConfiguration extends net.ihe.gazelle.simulator.sut.model.SystemConfiguration {

    private static final long serialVersionUID = 5093985945251904654L;
    /** Constant <code>_255="(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?"{trunked}</code> */
    public final static String _255 = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    /** Constant <code>_stringIPRegex_="^(?: + _255 + \\.){3} + _255 + $"</code> */
    public final static String _stringIPRegex_ = "^(?:" + _255 + "\\.){3}" + _255 + "$";
    /** Constant <code>PRIVATE_IP_REGEX="(^127\\.0\\.0\\.1)|(^10\\..*)|(^172\\.1"{trunked}</code> */
    public final static String PRIVATE_IP_REGEX = "(^127\\.0\\.0\\.1)|(^10\\..*)|(^172\\.1[6-9]\\..*)|(^172\\.2[0-9]\\..*)|(^172\\.3[0-1]\\..*)|(^192\\.168\\..*)";

    @Column(name = "application")
    @NotNull(message = "Please fill the application field!")
    private String application;

    @Column(name = "facility")
    @NotNull(message = "Please fill the facility field!")
    private String facility;

    @Column(name = "ip_address")
    @Pattern(regexp = _stringIPRegex_, message = "This IP Address is not valid !")
    private String ipAddress;

    @Column(name = "port")
    private Integer port;

    @Column(name = "private_ip")
    private Boolean privateIp;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "charset_id")
    @NotNull
    private Charset charset;

    @Column(name = "message_encoding")
    private MessageEncoding messageEncoding;

    @Column(name = "hl7_protocol")
    private HL7Protocol hl7Protocol;

    /**
     * <p>Constructor for HL7V2ResponderSUTConfiguration.</p>
     */
    public HL7V2ResponderSUTConfiguration() {
        this.privateIp = null;
        this.hl7Protocol = HL7Protocol.MLLP;
        this.messageEncoding = MessageEncoding.ER7;
    }

    /**
     * <p>Constructor for HL7V2ResponderSUTConfiguration.</p>
     *
     * @param selectedSystemConfiguration a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public HL7V2ResponderSUTConfiguration(HL7V2ResponderSUTConfiguration selectedSystemConfiguration) {
        super(selectedSystemConfiguration);
        this.application = selectedSystemConfiguration.getApplication();
        this.name = selectedSystemConfiguration.getName().concat("_COPY");
        this.facility = selectedSystemConfiguration.getFacility();
        this.ipAddress = selectedSystemConfiguration.getIpAddress();
        this.port = selectedSystemConfiguration.getPort();
        this.messageEncoding = selectedSystemConfiguration.getMessageEncoding();
        this.hl7Protocol = selectedSystemConfiguration.getHl7Protocol();
        this.url = selectedSystemConfiguration.getUrl();
        this.privateIp = selectedSystemConfiguration.getPrivateIp();
        this.charset = selectedSystemConfiguration.getCharset();
    }

    /**
     * <p>Getter for the field <code>application</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getApplication() {
        return application;
    }

    /**
     * <p>Setter for the field <code>application</code>.</p>
     *
     * @param application a {@link java.lang.String} object.
     */
    public void setApplication(String application) {
        this.application = application;
    }

    /**
     * <p>Getter for the field <code>facility</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFacility() {
        return facility;
    }

    /**
     * <p>Setter for the field <code>facility</code>.</p>
     *
     * @param facility a {@link java.lang.String} object.
     */
    public void setFacility(String facility) {
        this.facility = facility;
    }

    /**
     * <p>Getter for the field <code>ipAddress</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * <p>Setter for the field <code>ipAddress</code>.</p>
     *
     * @param ipAddress a {@link java.lang.String} object.
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * <p>Getter for the field <code>port</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getPort() {
        return port;
    }

    /**
     * <p>Setter for the field <code>port</code>.</p>
     *
     * @param port a {@link java.lang.Integer} object.
     */
    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     * <p>Getter for the field <code>privateIp</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getPrivateIp() {
        return privateIp;
    }

    /**
     * <p>Setter for the field <code>privateIp</code>.</p>
     *
     * @param privateIp a {@link java.lang.Boolean} object.
     */
    public void setPrivateIp(Boolean privateIp) {
        this.privateIp = privateIp;
    }

    /**
     * <p>Getter for the field <code>charset</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.Charset} object.
     */
    public Charset getCharset() {
        return charset;
    }

    /**
     * <p>Setter for the field <code>charset</code>.</p>
     *
     * @param charset a {@link net.ihe.gazelle.HL7Common.model.Charset} object.
     */
    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    /**
     * <p>Getter for the field <code>messageEncoding</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7.validator.client.MessageEncoding} object.
     */
    public MessageEncoding getMessageEncoding() {
        return messageEncoding;
    }

    /**
     * <p>Setter for the field <code>messageEncoding</code>.</p>
     *
     * @param messageEncoding a {@link net.ihe.gazelle.hl7.validator.client.MessageEncoding} object.
     */
    public void setMessageEncoding(MessageEncoding messageEncoding) {
        this.messageEncoding = messageEncoding;
    }

    /**
     * <p>Getter for the field <code>hl7Protocol</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.HL7Protocol} object.
     */
    public HL7Protocol getHl7Protocol() {
        return hl7Protocol;
    }

    /**
     * <p>Setter for the field <code>hl7Protocol</code>.</p>
     *
     * @param hl7Protocol a {@link net.ihe.gazelle.HL7Common.model.HL7Protocol} object.
     */
    public void setHl7Protocol(HL7Protocol hl7Protocol) {
        this.hl7Protocol = hl7Protocol;
    }

    /** {@inheritDoc} */
    @Override
    public String getEndpoint(){
        switch (this.hl7Protocol){
            case HTTP:
                return super.getUrl();
            case MLLP:
                return ipAddress + ":" + port;
            default:
                return super.getUrl();
        }
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HL7V2ResponderSUTConfiguration)) return false;
        if (!super.equals(o)) return false;

        HL7V2ResponderSUTConfiguration that = (HL7V2ResponderSUTConfiguration) o;

        if (application != null ? !application.equals(that.application) : that.application != null) return false;
        return !(facility != null ? !facility.equals(that.facility) : that.facility != null);

    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (application != null ? application.hashCode() : 0);
        result = 31 * result + (facility != null ? facility.hashCode() : 0);
        return result;
    }

    /**
     * <p>getAllConfigurationsForSelection.</p>
     *
     * @param transactionKeyword a {@link java.lang.String} object.
     * @param protocol a {@link net.ihe.gazelle.HL7Common.model.HL7Protocol} object.
     * @return a {@link java.util.List} object.
     */
    public static List<HL7V2ResponderSUTConfiguration> getAllConfigurationsForSelection(String transactionKeyword, HL7Protocol protocol){
        HL7V2ResponderSUTConfigurationQuery query = new HL7V2ResponderSUTConfigurationQuery();
        query.listUsages().transaction().keyword().eq(transactionKeyword);
        query.hl7Protocol().eq(protocol);
        query.name().order(true);
        if (!Identity.instance().isLoggedIn()) {
            query.isPublic().eq(true);
        } else if (!Identity.instance().hasRole("admin_role")) {

            GazelleIdentity gazelleIdentity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
            String institutionName = gazelleIdentity.getOrganisationKeyword();

            query.addRestriction(HQLRestrictions.or(query.ownerCompany().eqRestriction(institutionName), query.isPublic().eqRestriction(true)));
        }
        return query.getList();
    }

    /**
     * <p>getAllConfigurationsForUsage.</p>
     *
     * @param usageKeyword a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public static List<HL7V2ResponderSUTConfiguration> getAllConfigurationsForUsage(String usageKeyword){
        HL7V2ResponderSUTConfigurationQuery query = new HL7V2ResponderSUTConfigurationQuery();
        query.listUsages().keyword().eq(usageKeyword);
        query.name().order(true);
        return query.getList();
    }
}

package net.ihe.gazelle.HL7Common.responder;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.MetadataKeys;
import ca.uhn.hl7v2.protocol.ReceivingApplicationExceptionHandler;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.simulator.common.model.ReceiverConsole;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * <p>GazelleHL7ExceptionHandler class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class GazelleHL7ExceptionHandler implements ReceivingApplicationExceptionHandler {

    private static Logger log = LoggerFactory.getLogger(GazelleHL7ExceptionHandler.class);
    private SimulatorResponderConfiguration simulatorConfiguration = null;
    protected Map<String, Object> metadata;

    /**
     * <p>Constructor for GazelleHL7ExceptionHandler.</p>
     *
     * @param inConfiguration a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public GazelleHL7ExceptionHandler(SimulatorResponderConfiguration inConfiguration) {
        this.simulatorConfiguration = inConfiguration;
    }

    /**
     * <p>Constructor for GazelleHL7ExceptionHandler.</p>
     */
    public GazelleHL7ExceptionHandler() {

    }

    /**
     * <p>logException.</p>
     *
     * @param incomingMessage  a {@link java.lang.String} object.
     * @param incomingMetadata a {@link java.util.Map} object.
     * @param outgoingMessage  a {@link java.lang.String} object.
     * @param exception        a {@link java.lang.Exception} object.
     * @param entityManager    a {@link javax.persistence.EntityManager} object.
     *
     * @return a {@link java.lang.String} object.
     *
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public String logException(String incomingMessage, Map<String, Object> incomingMetadata,
                               String outgoingMessage, Exception exception, EntityManager entityManager) throws HL7Exception {

        if (incomingMessage == null) {
            return outgoingMessage;
        }
        this.metadata = incomingMetadata;
        String messageToReturn = outgoingMessage;
        Transaction unkTransaction = Transaction.GetTransactionByKeyword("UNKNOWN", entityManager);
        Actor unkActor = Actor.findActorWithKeyword("UNKNOWN", entityManager);
        Actor receivingActor = simulatorConfiguration.getSimulatedActor();
        Domain domain = simulatorConfiguration.getDomain();
        String sendingApplication = null;
        String sendingFacility = null;
        String receivingApplication = null;
        String receivingFacility = null;
        String receivedMessageType = null;
        String receivedControlId = null;
        String[] segments = incomingMessage.split("\\r");
        PipeParser parser = PipeParser.getInstanceWithNoValidation();
        Message receivedMessage = null;
        // extract information from incoming message manually
        if (segments.length > 0) {
            for (String currentSegment : segments) {
                if (currentSegment.startsWith("MSH")) {
                    receivedMessage = parser.parse(currentSegment);
                    Terser t = new Terser(receivedMessage);
                    sendingApplication = t.get("/.MSH-3-1");
                    sendingFacility = t.get("/.MSH-4-1");
                    receivingApplication = t.get("/.MSH-5-1");
                    receivingFacility = t.get("/.MSH-6-1");
                    receivedControlId = t.get("/.MSH-10");
                    receivedMessageType = buildMessageType(receivedMessage);
                    break;
                }
            }
        }
        Message response;
        if (messageToReturn == null && receivedMessage != null) {
            try {
                response = receivedMessage.generateACK(AcknowledgmentCode.AE, new HL7Exception(exception));
                messageToReturn = parser.encode(response);
            } catch (IOException e) {
                throw new HL7Exception(e);
            }
        } else {
            response = parser.parse(messageToReturn);

        }
        String sut = receivingFacility + "/" + receivingFacility;
        ReceiverConsole.newLog(receivingActor, unkTransaction, domain, receivedMessageType, receivedControlId, sut, null, exception.getMessage(),
                entityManager);
        TransactionInstance hl7Message = TransactionInstanceUtil.newInstance(sendingApplication,
                sendingFacility, receivingApplication, receivingFacility,
                incomingMessage, outgoingMessage, new Date(), unkTransaction, receivedMessageType, buildMessageType(response),
                receivingActor, unkActor, receivingActor, domain, getSenderIpAddress());
        hl7Message = hl7Message.save(entityManager);

        TransactionInstanceUtil.ExtractIds(receivedMessage, response, hl7Message);

        return messageToReturn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public String processException(final String incomingMessage, final Map<String, Object> incomingMetadata,
                                   final String outgoingMessage, final Exception exception) throws HL7Exception {
        try {
            Object response = HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                @Override
                public Object performAction(EntityManager entityManager, Object... context) throws Exception {
                    return logException(incomingMessage, incomingMetadata, outgoingMessage, exception, entityManager);
                }
            });
            if (response != null && response instanceof String) {
                return (String) response;
            }
        } catch (HibernateFailure e) {
            // already logged
        }
        return null;
    }

    /**
     * <p>buildMessageType.</p>
     *
     * @param message a {@link ca.uhn.hl7v2.model.Message} object.
     *
     * @return a {@link java.lang.String} object.
     */
    public String buildMessageType(Message message) {
        return HL7MessageDecoder.getMessageType(message);
    }

    /**
     * <p>Getter for the field <code>simulatorConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public SimulatorResponderConfiguration getSimulatorConfiguration() {
        return simulatorConfiguration;
    }

    /**
     * <p>Setter for the field <code>simulatorConfiguration</code>.</p>
     *
     * @param simulatorConfiguration a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public void setSimulatorConfiguration(SimulatorResponderConfiguration simulatorConfiguration) {
        this.simulatorConfiguration = simulatorConfiguration;
    }

    private String getSenderIpAddress() {
        if (this.metadata != null) {
            return (String) metadata.get(MetadataKeys.IN_SENDING_IP);
        } else {
            return null;
        }
    }
}

package net.ihe.gazelle.HL7Common.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>HL7MessageHighlighted class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HL7MessageHighlighted {


    /**
     * <p>highlightForHL7v2Message.</p>
     *
     * @param input a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String highlightForHL7v2Message(String input) {
        String messageContent = input;

        Integer b = 0;
        List<String> listSegment = new ArrayList<String>();
        listSegment.clear();

        Integer a = messageContent.indexOf("|");
        messageContent = messageContent.replace("<", "&lt;");
        messageContent = messageContent.replace(">", "&gt;");


        if ((a < messageContent.length()) && (a != -1)) {
            String subString = messageContent.substring(b, a);
            messageContent = messageContent.replaceFirst(subString, ("<span class=\"hl-reserved\">" + subString + "</span>"));

            while (a < messageContent.length() && a > 0 && b >= 0) {
                a = messageContent.indexOf("\r", a + 1);

                if ((a + 1 < messageContent.length()) && (a != -1)) {
                    b = messageContent.indexOf("|", a + 1);

                    if ((b - a) == 4) {
                        subString = messageContent.substring(a, b);

                        if (!listSegment.contains(subString)) {
                            listSegment.add(subString);
                            messageContent = messageContent.replaceAll(subString, ("\r<span class=\"hl-reserved\">" + subString.substring(1, 4) + "</span>"));
                        }
                    }
                } else
                    a = -1;
            }

        }

        messageContent = messageContent.replace("&", "<span class=\"hl-special\">&amp;</span>");
//		messageContent = messageContent.replace("~", "<span class=\"hl-special\">&tilde;</span>");  
        messageContent = messageContent.replace("#", "<span class=\"hl-special\">&#35;</span>");
        messageContent = messageContent.replace("~", "<span class=\"hl-special\">&#126;</span>");
        messageContent = messageContent.replace("|", "<span class=\"hl-special\">&#124;</span>");
        messageContent = messageContent.replace("^", "<span class=\"hl-brackets\">&#94;</span>");
        messageContent = messageContent.replace("\r", "<span class=\"hl-crlf\">[CR]</span><br/>");

        return messageContent;
    }


    /**
     * <p>highlightForHapiLog.</p>
     *
     * @param input a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String highlightForHapiLog(String input) {
        String hapiLogContent = input;

        if ((hapiLogContent != null) && (!hapiLogContent.isEmpty())) {
            hapiLogContent = "<br/> <span style=\"color:red\">Error Report : <br/></span>" + hapiLogContent + "<br/>";
            hapiLogContent = hapiLogContent.replace("\r", "<br/>");
        }

        return hapiLogContent;
    }


    /**
     * <p>highlightForEVSCLog.</p>
     *
     * @param input a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String highlightForEVSCLog(String input) {
        String evscLogContent = input;

        if ((evscLogContent != null) && (!evscLogContent.isEmpty())) {
            evscLogContent = "<br/> <span style=\"color:red\">Validation Report : <br/></span>" + evscLogContent + "<br/>";
            evscLogContent = evscLogContent.replace("\r", "<br/>");
        }

        return evscLogContent;
    }


}

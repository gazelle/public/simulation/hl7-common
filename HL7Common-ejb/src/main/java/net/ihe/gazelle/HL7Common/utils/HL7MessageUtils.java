package net.ihe.gazelle.HL7Common.utils;

import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.hl7.validator.client.MessageEncoding;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * <p>HL7MessageUtils class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HL7MessageUtils {


    /**
     * <p>exportToTxt.</p>
     *
     * @param content a {@link java.lang.String} object.
     * @param fileNameDestination a {@link java.lang.String} object.
     */
    public static void exportToTxt(String content, String fileNameDestination) {
        try {

            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse)
                    context.getExternalContext().getResponse();
            response.setContentType("text/plain");

            //if ( fileNameDestination == null ) fileNameDestination = ReportExporterManager.lastFileName(reportSource) ;
            response.setHeader("Content-Disposition", "attachment;filename=" + fileNameDestination);

            ServletOutputStream servletOutputStream;
            servletOutputStream = response.getOutputStream();
            servletOutputStream.write(content.getBytes());
            servletOutputStream.flush();
            servletOutputStream.close();

            context.responseComplete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * <p>getParserForMessageEncoding.</p>
     *
     * @param encoding a {@link net.ihe.gazelle.hl7.validator.client.MessageEncoding} object.
     * @return a {@link ca.uhn.hl7v2.parser.Parser} object.
     */
    public static Parser getParserForMessageEncoding(MessageEncoding encoding){
        Parser parser;
        switch (encoding) {
            case ER7:
                parser = PipeParser.getInstanceWithNoValidation();
                break;
            case XML:
                parser = DefaultXMLParser.getInstanceWithNoValidation();
                break;
            default:
                parser = PipeParser.getInstanceWithNoValidation();
                break;
        }
        return parser;
    }
}

package net.ihe.gazelle.HL7Common.exception;


/**
 * <p>CannotParseSegmentException class.</p>
 *
 * @author abe
 * @version 1.0: 15/04/19
 */

public class SegmentParsingException extends HL7v2ParsingException {


    public SegmentParsingException(String inMissingSegment, String errorLocation) {
        super(inMissingSegment, errorLocation);
    }

}

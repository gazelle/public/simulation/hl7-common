package net.ihe.gazelle.HL7Common.exception;

import ca.uhn.hl7v2.AcknowledgmentCode;

/**
 * <p>ReceivingApplicationUnavailableException class.</p>
 *
 * @author abe
 * @version 1.0: 16/04/19
 */

public class ReceivingFacilityUnavailableException extends HL7v2ParsingException {

    private static final String RECEIVING_FACILITY_LOCATION = "MSH^1^6";

    public ReceivingFacilityUnavailableException(String recevingFacilityFromMessage) {
        super(recevingFacilityFromMessage, RECEIVING_FACILITY_LOCATION);
    }


    @Override
    public String getErrorMessage(){
        return "Receiving facility unavailable: " + getElementInError();
    }

    @Override
    public AcknowledgmentCode getAckCode() {
        return AcknowledgmentCode.AR;
    }
}

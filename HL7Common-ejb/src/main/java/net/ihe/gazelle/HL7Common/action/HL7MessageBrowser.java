package net.ihe.gazelle.HL7Common.action;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.messages.HL7Validator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.model.ValidationParameters;
import net.ihe.gazelle.HL7Common.model.ValidatorType;
import net.ihe.gazelle.HL7Common.utils.HL7MessageUtils;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.common.util.XmlFormatter;
import net.ihe.gazelle.hl7.MessageDisplay;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.message.action.AbstractTransactionInstanceDisplay;
import net.ihe.gazelle.simulator.message.model.EStandard;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.ws.TestReport;
import org.dom4j.DocumentException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>HL7MessageBrowser class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("hl7MessageBrowser")
@Scope(ScopeType.PAGE)
public class HL7MessageBrowser extends AbstractTransactionInstanceDisplay {

    /**
     *
     */
    private static final long serialVersionUID = 236053079070523657L;
    private static final int DEFAULT_TIMEOUT = 5000;
    private static final int FIELD_RESP_APP = 5;
    private static final int FIELD_RESP_FAC = 6;
    private static final int FIELD_MESSAGE_CONTROL_ID = 10;
    private static final int FIELD_TIMESTAMP = 7;
    private static final int FIRST_OCCURENCE = 0;
    private static final int FIRST_COMPONENT = 1;
    private static final int FIRST_SUB_COMPONENT = 1;
    private static Logger log = LoggerFactory.getLogger(HL7MessageBrowser.class);

    private static Transformer transformerForV2 = null;
    private static Transformer transformerForV3 = null;


    private static void initializeTransformers() {
        createTransformerForV2();
        createTransformerForV3();
    }

    private static void createTransformerForV2() {
        if (transformerForV2 == null) {
            transformerForV2 = createTransformerFromUrl("hl7v2_xsl_location");
        }
    }

    private static void createTransformerForV3() {
        if (transformerForV3 == null) {
            transformerForV3 = createTransformerFromUrl("hl7v3_validation_xsl_location");
        }
        if (transformerForV3 != null) {
            transformerForV3.setParameter("ShowGeneralInformation", false);
        }
    }

    private static Transformer createTransformerFromUrl(String preferenceName) {
        TransformerFactory factory = TransformerFactory.newInstance("org.apache.xalan.processor.TransformerFactoryImpl", null);
        try {
            URL url = new URL(PreferenceService.getString(preferenceName));
            URLConnection connection = url.openConnection();
            connection.setConnectTimeout(DEFAULT_TIMEOUT);
            connection.setReadTimeout(DEFAULT_TIMEOUT);
            InputStream xsl = connection.getInputStream();
            Transformer transformer = factory.newTransformer(new StreamSource(xsl));
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            return transformer;
        } catch (TransformerConfigurationException e) {
            log.error("Cannot instantiate transformer: {}", e.getMessage());
            return null;
        } catch (Exception e) {
            log.info("Unable to set up the transformer for validation results using {}: {}", preferenceName, e.getMessage());
            return null;
        }
    }

    /**
     * <p>onCreate.</p>
     */
    @Create
    public void onCreate() {
        initializeTransformers();
        super.getTransactionInstanceFromUrl();
    }

    /**
     * <p>isHL7v2.</p>
     *
     * @return a boolean.
     */
    public boolean isHL7v2() {
        return (selectedInstance.getStandard() != null && selectedInstance.getStandard().equals(EStandard.HL7V2));
    }

    /**
     * <p>isHL7v3.</p>
     *
     * @return a boolean.
     */
    public boolean isHL7v3() {
        return (selectedInstance.getStandard() != null && selectedInstance.getStandard().equals(EStandard.HL7V3));
    }

    /**
     * <p>isFhir.</p>
     *
     * @return a boolean.
     */
    public boolean isFhir() {
        return isFhirJson() || isFhirXml();
    }

    /**
     * <p>isFhirXml.</p>
     *
     * @return a boolean.
     */
    public boolean isFhirXml() {
        return (selectedInstance.getStandard() != null && selectedInstance.getStandard().equals(EStandard.FHIR_XML));
    }

    /**
     * <p>isFhirJson.</p>
     *
     * @return a boolean.
     */
    public boolean isFhirJson() {
        return (selectedInstance.getStandard() != null && selectedInstance.getStandard().equals(EStandard.FHIR_JSON));
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String getPrettyFormattedResult(MessageInstance messageInstance) {
        // If the transaction have a DetailedResult
        if (messageInstance.getValidationDetailedResult() != null) {
            if (EStandard.HL7V2.equals(selectedInstance.getStandard())) {
                return xmlToHtml(messageInstance.getValidationDetailedResult(), transformerForV2);
            } else {
                return xmlToHtml(messageInstance.getValidationDetailedResult(), transformerForV3);
            }
        }
        return "";
    }

    /**
     * <p>printMessage.</p>
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
     */

    public void printMessage(MessageInstance instance) {
        HL7MessageUtils.exportToTxt(instance.getContentAsString(), ("Message_" + instance.getType() + ".txt"));
    }


    /**
     * <p>displayFormattedJSON.</p>
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
     *
     * @return a {@link java.lang.String} object.
     */
    public String displayFormattedJSON(MessageInstance instance) {
        if ((instance != null) && (instance.getContent() != null) && selectedInstance.getStandard() != null) {
            if (EStandard.FHIR_JSON.equals(selectedInstance.getStandard())) {
                return fhirToJson(instance);
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    private String fhirToJson(MessageInstance instance) {
        return instance.getContentAsString();

    }


    /**
     * <p>displayFormattedJSONPayload.</p>
     * @param instance a byte
     * @return a {@link java.lang.String} object.
     */
    public String displayFormattedJSONPayload(byte[] instance) {
        return (instance != null && EStandard.FHIR_JSON.equals(selectedInstance.getStandard())) ? new String (instance, StandardCharsets.UTF_8) : "";
    }


    /**
     * <p>displayFormattedXMLPayload.</p>
     * @param instance a Byte
     * @return a {@link java.lang.String} object.
     * @throws IOException
     * @throws DocumentException
     */
    public String displayFormattedXMLPayload(byte[] instance) throws IOException, DocumentException {
        return (instance != null) ? XmlFormatter.format(new String(instance, StandardCharsets.UTF_8)) : "";
    }


    /**
     * <p>displayFormattedXML.</p>
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
     *
     * @return a {@link java.lang.String} object.
     */
    public String displayFormattedXML(MessageInstance instance) {
        if ((instance != null) && (instance.getContent() != null) && selectedInstance.getStandard() != null) {
            switch (selectedInstance.getStandard()) {
                case HL7V3:
                    return hl7v3ToXml(instance);
                case HL7V2:
                    return hl7v2ToXml(instance);
                case FHIR_XML:
                    return fhirToXml(instance);
                default:
                    return "";
            }
        } else {
            return "";
        }
    }

    private String fhirToXml(MessageInstance instance) {
        try {
            return XmlFormatter.format(instance.getContentAsString());
        } catch (Exception e) {
            return instance.getContentAsString();
        }
    }

    private String hl7v3ToXml(MessageInstance instance) {
        try {
            return XmlFormatter.format(instance.getContentAsString());
        } catch (Exception e) {
            return instance.getContentAsString();
        }
    }

    private String hl7v2ToXml(MessageInstance instance) {
        try {
            Message message = stringToHapiMessage(instance);
            if (message != null) {
                return MessageDisplay.getMessageContentAsXML(message);
            } else {
                return null;
            }
        } catch (HL7Exception e) {
            log.error(e.getMessage());
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The tool was not able to convert the message into XML");
            return "";
        }
    }

    private Message stringToHapiMessage(MessageInstance instance) throws HL7Exception {
        Parser pipeParser = PipeParser.getInstanceWithNoValidation();
        Message message;
        ValidationParameters validationParameters = TransactionInstanceUtil.getValidatorForRequest(selectedInstance);
        if (validationParameters != null && validationParameters.getJavaPackage() != null) {
            message = pipeParser.parseForSpecificPackage(instance.getContentAsString(), validationParameters.getJavaPackage());
        } else {
            message = pipeParser.parse(instance.getContentAsString());
        }
        return message;
    }

    /**
     * <p>displayFormattedHTML.</p>
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
     *
     * @return a {@link java.lang.String} object.
     */
    public String displayFormattedHTML(MessageInstance instance) {
        if ((instance != null) && (instance.getContent() != null) && selectedInstance.getStandard() != null) {
            switch (selectedInstance.getStandard()) {
                case HL7V2:
                    return highlightER7(instance);
                default:
                    return "";
            }
        } else {
            return "";
        }
    }

    private String highlightER7(MessageInstance instance) {
        return MessageDisplay.getMessageContentAsHighlightedER7(instance.getContentAsString());
    }

    private String xmlToHtml(byte[] xml, Transformer transformer) {
        if (transformer == null) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "The system has not been able to apply the stylesheet");
            return new String(xml, StandardCharsets.UTF_8);
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        StreamResult out = new StreamResult(baos);
        InputStream is = new ByteArrayInputStream(xml);
        try {
            transformer.transform(new javax.xml.transform.stream.StreamSource(is), out);
        } catch (TransformerException e) {
            log.error(e.getMessage());
            return null;
        }
        String tmp = new String(baos.toByteArray());
        try {
            baos.close();
            is.close();
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
        return tmp;
    }

    /**
     * <p>getRawMessage.</p>
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRawMessage(MessageInstance instance) {
        return MessageDisplay.getMessageContentAsER7(instance.getContentAsString());
    }

    /**
     * <p>getGetRequest.</p>
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
     *
     * @return a {@link java.lang.String} object.
     */
    public String getGetRequest(MessageInstance instance) {
        String request = instance.getContentAsString();
        if (request != null) {
            try {
                return URLDecoder.decode(request, StandardCharsets.UTF_8.name());
            } catch (UnsupportedEncodingException e) {
                return request;
            }
        }
        return "";
    }

    /**
     * <p>getTree.</p>
     *
     * @param instance a {@link net.ihe.gazelle.simulator.message.model.MessageInstance} object.
     *
     * @return a {@link org.richfaces.model.TreeNode} object.
     */
    public TreeNode getTree(MessageInstance instance) {
        if (selectedInstance.getStandard() != null && selectedInstance.getStandard().equals(EStandard.HL7V2)) {
            try {
                Message message = stringToHapiMessage(instance);
                if (message != null) {
                    return MessageDisplay.getMessageContentAsTree(message, null);
                } else {
                    return null;
                }
            } catch (HL7Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The tool was not able to parse the message");
            } catch (SAXException | IOException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The tool was not able to build the tree");
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String permanentLinkToTestReport() {
        return TestReport.buildTestReportRestUrl(selectedInstance.getId(), null);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void validateResponse() {
        if (selectedInstance.getResponse() != null && !selectedInstance.getResponse().isEmpty()) {
            selectedInstance = HL7Validator.validateResponse(selectedInstance);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "No response content registered");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateRequest() {
        if (selectedInstance.getRequest() != null && !selectedInstance.getRequest().isEmpty()) {
            selectedInstance = HL7Validator.validateRequest(selectedInstance);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "No request content registered");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateResponse(TransactionInstance ti) {
        selectedInstance = ti;
        validateResponse();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateRequest(TransactionInstance ti) {
        selectedInstance = ti;
        validateRequest();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replayMessage() {
        if (selectedInstance != null && sutForReplay != null) {
            if (sutForReplay instanceof HL7V2ResponderSUTConfiguration) {
                HL7V2ResponderSUTConfiguration v2Responder = (HL7V2ResponderSUTConfiguration) sutForReplay;
                Message message = updateMessage(selectedInstance.getRequest(), v2Responder);
                if (message != null) {
                    String facility = getFacilityFromMessage(message);
                    String application = getApplicationFromMessage(message);
                    PipeParser parser = PipeParser.getInstanceWithNoValidation();
                    try {
                        String messageString = parser.encode(message);
                        Initiator initiator = new Initiator(v2Responder, facility, application, selectedInstance.getSimulatedActor(),
                                selectedInstance.getTransaction(), messageString, selectedInstance.getRequest().getType(),
                                selectedInstance.getDomain().getKeyword(), selectedInstance.getResponse().getIssuingActor());
                        initiator.sendMessage();
                    } catch (HL7Exception e) {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The tool has not been able to parse the message");
                    }
                }
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, "Sorry, this tool is not yet able to replay HL7V3 messages");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please select a system in the drop-down list and retry");
        }
    }

    private String getApplicationFromMessage(Message message) {
        Terser terser = new Terser(message);
        try {
            return terser.get("/.MSH-3-1");
        } catch (HL7Exception e) {
            return null;
        }
    }

    private String getFacilityFromMessage(Message message) {
        Terser terser = new Terser(message);
        try {
            return terser.get("/.MSH-4-1");
        } catch (HL7Exception e) {
            return null;
        }
    }

    private Message updateMessage(MessageInstance instance, HL7V2ResponderSUTConfiguration v2Responder) {
        PipeParser parser = PipeParser.getInstanceWithNoValidation();
        try {
            Message message = parser.parse(instance.getContentAsString());
            Terser terser = new Terser(message);
            Segment msh = terser.getSegment("/.MSH");
            Terser.set(msh, FIELD_RESP_APP, FIRST_OCCURENCE, FIRST_COMPONENT, FIRST_SUB_COMPONENT, v2Responder.getApplication());
            Terser.set(msh, FIELD_RESP_FAC, FIRST_OCCURENCE, FIRST_COMPONENT, FIRST_SUB_COMPONENT, v2Responder.getFacility());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String date = sdf.format(new Date());
            Terser.set(msh, FIELD_MESSAGE_CONTROL_ID, FIRST_OCCURENCE, FIRST_COMPONENT, FIRST_SUB_COMPONENT, date);
            Terser.set(msh, FIELD_TIMESTAMP, FIRST_OCCURENCE, FIRST_COMPONENT, FIRST_SUB_COMPONENT, date);
            return message;
        } catch (HL7Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The tool has not been able to parse the message");
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCloseMessagePopup() {
        super.onCloseMessagePopup();
    }


}

package net.ihe.gazelle.HL7Common.exception;

import net.ihe.gazelle.HL7Common.utils.HL7Constants;

/**
 * <p>MissingRequiredSegmentException class.</p>
 *
 * @author abe
 * @version 1.0: 15/04/19
 */

public class UnknownKeyIdentifierException extends HL7v2ParsingException {

    private String errorMessage;


    public UnknownKeyIdentifierException(String inUnknownElement, String errorLocation) {
        super(inUnknownElement, errorLocation);
        setErrorMessage("Cannot find element with identifier: " + inUnknownElement);
    }

    public UnknownKeyIdentifierException(String inUnknownElement, String message, String errorLocation) {
        super(inUnknownElement, errorLocation);
        setErrorMessage(message);
    }

    @Override
    public String getHL7ErrorCode() {
        return HL7Constants.UNKNOWN_KEY_IDENTIFIER;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

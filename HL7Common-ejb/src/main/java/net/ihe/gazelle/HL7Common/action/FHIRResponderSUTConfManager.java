package net.ihe.gazelle.HL7Common.action;

import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfigurationQuery;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <p>FHIRResponderSUTConfManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("fhirResponderSUTConfManager")
@Scope(ScopeType.PAGE)
public class FHIRResponderSUTConfManager extends
        AbstractSystemConfigurationManager<FHIRResponderSUTConfiguration> implements Serializable, UserAttributeCommon {

    private static final long serialVersionUID = -8203169928429200398L;

    @In(value = "gumUserService")
    private UserService userService;

    GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteSelectedSystemConfiguration() {
        if (selectedSystemConfiguration != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            FHIRResponderSUTConfiguration configToRemove = entityManager.find(
                    FHIRResponderSUTConfiguration.class, selectedSystemConfiguration.getId());
            try {
                entityManager.remove(configToRemove);
                entityManager.flush();
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "SUT configuration successfully deleted");
            } catch (Exception e) {
                String msg = "Could not delete selected system under test because a preference is attached to it. " +
                        "You must delete this preference via the \"SUT's assigning authorities page\"";
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg);
            }
            selectedSystemConfiguration = null;
            reset();
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No SUT selected for deletion");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected HQLCriterionsForFilter<FHIRResponderSUTConfiguration> getHQLCriterionsForFilter() {
        FHIRResponderSUTConfigurationQuery query = new FHIRResponderSUTConfigurationQuery();
        HQLCriterionsForFilter<FHIRResponderSUTConfiguration> criterionsForFilter = query.getHQLCriterionsForFilter();
        criterionsForFilter.addPath("owner", query.owner());
        criterionsForFilter.addQueryModifier(this);
        return criterionsForFilter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void copySelectedConfiguration(FHIRResponderSUTConfiguration inConfiguration) {
        if (inConfiguration != null) {
            selectedSystemConfiguration = new FHIRResponderSUTConfiguration(inConfiguration);
            editSelectedConfiguration();
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "No configuration selected");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addConfiguration() {
        selectedSystemConfiguration = new FHIRResponderSUTConfiguration();
        displayEditPanel = true;
        displayListPanel = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Usage> getPossibleListUsages() {
        UsageQuery query = new UsageQuery();
        List<Usage> usages = query.getList();
        Collections.sort(usages);
        return usages;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyQuery(HQLQueryBuilder<FHIRResponderSUTConfiguration> hqlQueryBuilder, Map<String, Object> map) {
        FHIRResponderSUTConfigurationQuery query = new FHIRResponderSUTConfigurationQuery();
        if (!Identity.instance().isLoggedIn()) {
            hqlQueryBuilder.addRestriction(query.isPublic().eqRestriction(true));
        } else if (!Identity.instance().hasRole("admin_role")) {
            hqlQueryBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
                    HQLRestrictions.eq("ownerCompany", identity.getOrganisationKeyword())));
        }
        if (filteredUsage != null) {
            hqlQueryBuilder.addRestriction(query.listUsages().id().eqRestriction(filteredUsage.getId()));
        }
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}

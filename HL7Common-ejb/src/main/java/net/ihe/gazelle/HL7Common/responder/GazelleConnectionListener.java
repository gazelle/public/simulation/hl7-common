package net.ihe.gazelle.HL7Common.responder;

import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.ConnectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>GazelleConnectionListener class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class GazelleConnectionListener implements ConnectionListener {


    private static Logger log = LoggerFactory.getLogger(GazelleConnectionListener.class);

    /** {@inheritDoc} */
    @Override
    public void connectionReceived(Connection c) {
        log.info("Connection received from " + c.getRemoteAddress().getHostAddress());
    }

    /** {@inheritDoc} */
    @Override
    public void connectionDiscarded(Connection c) {
        log.info("Connection closed by foreign host (" + c.getRemoteAddress().getHostAddress() + ")");
    }

}

package net.ihe.gazelle.HL7Common.exception;


import ca.uhn.hl7v2.AcknowledgmentCode;
import net.ihe.gazelle.HL7Common.utils.HL7Constants;

/**
 * <p>MissingRequiredSegmentException class.</p>
 *
 * @author abe
 * @version 1.0: 15/04/19
 */

public class MissingRequiredFieldException extends HL7v2ParsingException {


    public MissingRequiredFieldException(String inMissingSegment, String errorLocation) {
        super(inMissingSegment, errorLocation);
    }

    @Override
    public String getHL7ErrorCode(){
       return HL7Constants.REQUIRED_FIELD_MISSING;
    }

    @Override
    public String getErrorMessage(){
        return getElementInError() + " is required but missing";
    }

    @Override
    public AcknowledgmentCode getAckCode() {
        return AcknowledgmentCode.AR;
    }

}

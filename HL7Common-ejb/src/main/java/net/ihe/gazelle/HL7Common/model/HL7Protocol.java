package net.ihe.gazelle.HL7Common.model;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>HL7Protocol class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public enum HL7Protocol {

    HTTP("HL7 over HTTP"),
    MLLP("Minimum Lower Layer Protocol");

    String value;

    private HL7Protocol(String value) {
        this.value = value;
    }

    /**
     * <p>Getter for the field <code>value</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValue() {
        return this.value;
    }

    /**
     * <p>getListForSelectMenu.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public static List<SelectItem> getListForSelectMenu() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (HL7Protocol protocol : values()) {
            items.add(new SelectItem(protocol, protocol.getValue()));
        }
        return items;
    }
}

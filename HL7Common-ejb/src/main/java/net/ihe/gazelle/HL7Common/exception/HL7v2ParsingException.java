package net.ihe.gazelle.HL7Common.exception;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.HL7Common.utils.HL7Constants;

/**
 * <p>HL7v2ParsingException class.</p>
 *
 * @author abe
 * @version 1.0: 15/04/19
 */

public class HL7v2ParsingException extends Throwable {

    private String elementInError;
    private String errorLocation;

    public HL7v2ParsingException(String inMissingSegment, String errorLocation){
        this.elementInError = inMissingSegment;
        this.errorLocation = errorLocation;
    }

    public HL7v2ParsingException(String inElementName, String errorMessage, String errorLocation){
        super(errorMessage);
        this.elementInError = inElementName;
        this.errorLocation = errorLocation;
    }

    public HL7v2ParsingException(HL7Exception e) {
        super(e);
        this.errorLocation = null;
    }

    public HL7v2ParsingException(String errorMessage) {
        super(errorMessage);
    }

    public String getElementInError() {
        return elementInError;
    }

    public String getHL7ErrorCode(){
        return HL7Constants.INTERNAL_ERROR;
    }

    public String getErrorMessage(){
        return getMessage();
    }

    public AcknowledgmentCode getAckCode() {
        return AcknowledgmentCode.AE;
    }

    public String getErrorLocation() {
        return errorLocation;
    }
}

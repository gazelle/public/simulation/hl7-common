package net.ihe.gazelle.HL7Common.action;

import net.ihe.gazelle.HL7Common.model.ValidationParameters;
import net.ihe.gazelle.HL7Common.model.ValidationParametersQuery;
import net.ihe.gazelle.HL7Common.model.ValidatorType;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.xml.validation.Validator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by xfs on 03/05/16.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Name("validationParametersManager")
@Scope(ScopeType.PAGE)
public class ValidationParametersManager implements Serializable{

    private static final long serialVersionUID = 1L;
    private ValidationParameters validationParameters;
    private Filter<ValidationParameters> filter;

    /**
     * <p>Getter for the field <code>validationParameters</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.ValidationParameters} object.
     */
    public ValidationParameters getValidationParameters() {
        return validationParameters;
    }

    /**
     * <p>Setter for the field <code>validationParameters</code>.</p>
     *
     * @param validationParameters a {@link net.ihe.gazelle.HL7Common.model.ValidationParameters} object.
     */
    public void setValidationParameters(ValidationParameters validationParameters) {
        this.validationParameters = validationParameters;
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<ValidationParameters> getFilter() {
        if (filter == null) {
            filter = new Filter<ValidationParameters>(getHQLCriteriaForFilters());
        }
        return filter;
    }

    /**
     * <p>Setter for the field <code>filter</code>.</p>
     *
     * @param filter a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public void setFilter(Filter<ValidationParameters> filter) {
        this.filter = filter;
    }

    private HQLCriterionsForFilter<ValidationParameters> getHQLCriteriaForFilters() {
        ValidationParametersQuery query = new ValidationParametersQuery();
        HQLCriterionsForFilter<ValidationParameters> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("profileOID", query.profileOID());
        return criteria;
    }

    /**
     * <p>getValidationParametersList.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<ValidationParameters> getValidationParametersList() {
        return new FilterDataModel<ValidationParameters>(getFilter()) {
            @Override
            protected Object getId(ValidationParameters validationParameter) {
                return validationParameter.getId();
            }
        };
    }

    /**
     * <p>viewValidationParameter.</p>
     *
     * @param validationParameterId a int.
     * @return a {@link java.lang.String} object.
     */
    public String viewValidationParameter(int validationParameterId){
        return ApplicationConfiguration.getValueOfVariable("application_url") + "/configuration/viewValidationParameters.seam?id="+validationParameterId;
    }

    /**
     * <p>editValidationParameter.</p>
     *
     * @param validationParameterId a int.
     * @return a {@link java.lang.String} object.
     */
    public String editValidationParameter(int validationParameterId){
        return ApplicationConfiguration.getValueOfVariable("application_url") + "/configuration/editValidationParameters.seam?id="+validationParameterId;
    }

    /**
     * <p>initValidationParameters.</p>
     */
    public void initValidationParameters() {
        validationParameters = new ValidationParameters();
        validationParameters.setValidatorType(ValidatorType.V2);
    }

    /**
     * <p>getValidationParametersFromUrl.</p>
     */
    public void getValidationParametersFromUrl() {
        EntityManager em = EntityManagerService.provideEntityManager();
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey("id")) {
            try {
                validationParameters = em.find(ValidationParameters.class, Integer.parseInt(urlParams.get("id")));
            }catch (NumberFormatException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, urlParams.get("id") + " is not a valid ValidationParameters ID.");
            }
        }else{
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "missing id parameter in URL");
        }
    }

    /**
     * <p>getValidatorTypes.</p>
     *
     * @return an array of {@link net.ihe.gazelle.HL7Common.model.ValidatorType} objects.
     */
    public List<SelectItem> getValidatorTypes(){
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (ValidatorType type: ValidatorType.values()) {
            items.add(new SelectItem(type, type.getLabel()));
        }
        return items;
    }

    /**
     * <p>saveValidationParameters.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String saveValidationParameters(){
        EntityManager em = EntityManagerService.provideEntityManager();
        validationParameters = em.merge(validationParameters);
        em.flush();
        return "/configuration/validationParametersList.seam";
    }

    /**
     * <p>setSelectedValidationParameters.</p>
     *
     * @param currentValidationParameters a {@link net.ihe.gazelle.HL7Common.model.ValidationParameters} object.
     */
    public void setSelectedValidationParameters(ValidationParameters currentValidationParameters){
        this.validationParameters = currentValidationParameters;
    }

    /**
     * <p>deleteSelectedValidationParameters.</p>
     */
    public void deleteSelectedValidationParameters(){
        deleteValidationParameters(this.validationParameters);
    }

    private void deleteValidationParameters(ValidationParameters validationParameters) {
        EntityManager em = EntityManagerService.provideEntityManager();
        validationParameters = em.find(ValidationParameters.class, validationParameters.getId());
        em.remove(validationParameters);
        em.flush();
        FacesMessages.instance().add(StatusMessage.Severity.INFO,"Validation Parameters " + validationParameters.getId() + " deleted");
    }

    public List<Actor> listAllActors(){
        return Actor.listAllActors();
    }

    public List<Domain> listAllDomains(){
        return Domain.listAllDomains();
    }

    public List<Transaction> listAllTransactions(){
        return Transaction.ListAllTransactions();
    }

}

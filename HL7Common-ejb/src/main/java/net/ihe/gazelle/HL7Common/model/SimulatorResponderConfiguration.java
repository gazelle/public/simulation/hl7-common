package net.ihe.gazelle.HL7Common.model;

import net.ihe.gazelle.hl7.validator.client.MessageEncoding;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.sql.BatchUpdateException;
import java.util.List;

/**
 * Class description : <b>SimulatorResponderConfiguration</b> This class describes configuration for a simulator acting as HL7 responder
 * <p/>
 * This class contains the following attributes:
 * <ul>
 * <li><b>Id</b> Id in the database</li>
 * <li><b>name</b> The name given to the configuration</li>
 * <li><b>listeningPort</b> The port on which the simulator is listening</li>
 * <li><b>simulatedActor</b> The simulated actor</li>
 * <li><b>transaction</b> The transaction handled by this simulator. This field can be null if a unique handler is used for all transactions for this actor</li>
 * <li><b>domain</b> The affinity domain supported by this simulator. It can be null if a unique handler is used for all transactions</li>
 * <li><b>charset</b> The charset to use when opening the socket for receiving messages <b>(deprecated since the use of hapi-2.0)</b></li>
 * <li><b>acceptedMessageType</b> The message type accepted by this simulator, can be *</li>
 * <li><b>acceptedTriggerEvent</b> The trigger event accepted by this simulator, can be *</li>
 * <li><b>serverBeanName</b> name of the application bean used to start the service.</li>
 * </ul>
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes - Bretagne Atlantique
 * @version 1.0 - 2011, July 8th
 */

@Entity
@Name("responderConfiguration")
@Table(name = "hl7_simulator_responder_configuration", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@SequenceGenerator(name = "hl7_simulator_responder_configuration_sequence", sequenceName = "hl7_simulator_responder_configuration_id_seq", allocationSize = 1)
public class SimulatorResponderConfiguration implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7386760702698088878L;

    private static final String WILD_CHAR = "*";
    /** Constant <code>_255="(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?"{trunked}</code> */
    public final static String _255 = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    /** Constant <code>_stringIPRegex_="^(?: + _255 + \\.){3} + _255 + $"</code> */
    public final static String _stringIPRegex_ = "^(?:" + _255 + "\\.){3}" + _255 + "$";

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "hl7_simulator_responder_configuration_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "name", unique = true)
    @NotNull
    private String name;

    @Column(name = "listening_port")
    private int listeningPort;

    @Column(name = "ip_address")
    @Pattern(regexp = _stringIPRegex_, message = "This IP Address is not valid !")
    private String ipAddress;

    @ManyToOne
    @JoinColumn(name = "simulated_actor_id")
    @NotNull
    private Actor simulatedActor;

    @ManyToOne
    @JoinColumn(name = "transaction_id")
    private Transaction transaction;

    @ManyToOne
    @JoinColumn(name = "domain_id")
    private Domain domain;

    @Column(name = "accepted_message_type")
    private String acceptedMessageType;

    @Column(name = "accepted_trigger_event")
    private String acceptedTriggerEvent;

    @Column(name = "server_bean_name")
    private String serverBeanName;

    @Column(name = "receiving_facility")
    @NotNull
    private String receivingFacility;

    @Column(name = "receiving_application")
    @NotNull
    private String receivingApplication;

    @Column(name = "is_running")
    private Boolean running;

    @Column(name = "message_encoding")
    private MessageEncoding messageEncoding;

    @Column(name = "hl7_protocol")
    private HL7Protocol hl7Protocol;

    @Column(name = "url")
    private String url;

    /**
     * <p>Constructor for SimulatorResponderConfiguration.</p>
     */
    public SimulatorResponderConfiguration() {
        this.running = false;
    }

    /**
     * Constructor for copy
     *
     * @param inConfiguration a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public SimulatorResponderConfiguration(SimulatorResponderConfiguration inConfiguration) {
        this.name = inConfiguration.getName().concat("_COPY");
        this.acceptedMessageType = inConfiguration.getAcceptedMessageType();
        this.acceptedTriggerEvent = inConfiguration.getAcceptedTriggerEvent();
        this.transaction = inConfiguration.getTransaction();
        this.simulatedActor = inConfiguration.getSimulatedActor();
        this.domain = inConfiguration.getDomain();
        this.serverBeanName = inConfiguration.getServerBeanName();
        this.receivingApplication = inConfiguration.getReceivingApplication();
        this.receivingFacility = inConfiguration.getReceivingFacility();
        this.ipAddress = inConfiguration.getIpAddress();
        this.running = false;
        this.messageEncoding = inConfiguration.getMessageEncoding();
        this.hl7Protocol = inConfiguration.getHl7Protocol();
        this.url = inConfiguration.getUrl();
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>listeningPort</code>.</p>
     *
     * @return a int.
     */
    public int getListeningPort() {
        return listeningPort;
    }

    /**
     * <p>Setter for the field <code>listeningPort</code>.</p>
     *
     * @param listeningPort a int.
     */
    public void setListeningPort(int listeningPort) {
        this.listeningPort = listeningPort;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Setter for the field <code>simulatedActor</code>.</p>
     *
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    /**
     * <p>Getter for the field <code>transaction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * <p>Setter for the field <code>transaction</code>.</p>
     *
     * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    /**
     * <p>Getter for the field <code>domain</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public Domain getDomain() {
        return domain;
    }

    /**
     * <p>Setter for the field <code>domain</code>.</p>
     *
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    /**
     * <p>Getter for the field <code>acceptedMessageType</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAcceptedMessageType() {
        if (acceptedMessageType == null || acceptedMessageType.isEmpty()) {
            return WILD_CHAR;
        } else {
            return acceptedMessageType;
        }
    }

    /**
     * <p>Setter for the field <code>acceptedMessageType</code>.</p>
     *
     * @param acceptedMessageType a {@link java.lang.String} object.
     */
    public void setAcceptedMessageType(String acceptedMessageType) {
        this.acceptedMessageType = acceptedMessageType;
    }

    /**
     * <p>Getter for the field <code>acceptedTriggerEvent</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAcceptedTriggerEvent() {
        if (acceptedTriggerEvent == null || acceptedTriggerEvent.isEmpty()) {
            return WILD_CHAR;
        } else {
            return acceptedTriggerEvent;
        }
    }

    /**
     * <p>Setter for the field <code>acceptedTriggerEvent</code>.</p>
     *
     * @param acceptedTriggerEvent a {@link java.lang.String} object.
     */
    public void setAcceptedTriggerEvent(String acceptedTriggerEvent) {
        this.acceptedTriggerEvent = acceptedTriggerEvent;
    }

    /**
     * <p>Setter for the field <code>serverBeanName</code>.</p>
     *
     * @param serverBeanName a {@link java.lang.String} object.
     */
    public void setServerBeanName(String serverBeanName) {
        this.serverBeanName = serverBeanName;
    }

    /**
     * <p>Getter for the field <code>serverBeanName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getServerBeanName() {
        return serverBeanName;
    }

    /**
     * <p>Getter for the field <code>receivingFacility</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getReceivingFacility() {
        return receivingFacility;
    }

    /**
     * <p>Setter for the field <code>receivingFacility</code>.</p>
     *
     * @param receivingFacility a {@link java.lang.String} object.
     */
    public void setReceivingFacility(String receivingFacility) {
        this.receivingFacility = receivingFacility;
    }

    /**
     * <p>Getter for the field <code>receivingApplication</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getReceivingApplication() {
        return receivingApplication;
    }

    /**
     * <p>Setter for the field <code>receivingApplication</code>.</p>
     *
     * @param receivingApplication a {@link java.lang.String} object.
     */
    public void setReceivingApplication(String receivingApplication) {
        this.receivingApplication = receivingApplication;
    }

    /**
     * <p>Setter for the field <code>ipAddress</code>.</p>
     *
     * @param ipAddress a {@link java.lang.String} object.
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * <p>Getter for the field <code>ipAddress</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * <p>Setter for the field <code>running</code>.</p>
     *
     * @param running a {@link java.lang.Boolean} object.
     */
    public void setRunning(Boolean running) {
        this.running = running;
    }

    /**
     * <p>Getter for the field <code>running</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getRunning() {
        return running;
    }

    /**
     * <p>Getter for the field <code>messageEncoding</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7.validator.client.MessageEncoding} object.
     */
    public MessageEncoding getMessageEncoding() {
        return messageEncoding;
    }

    /**
     * <p>Setter for the field <code>messageEncoding</code>.</p>
     *
     * @param messageEncoding a {@link net.ihe.gazelle.hl7.validator.client.MessageEncoding} object.
     */
    public void setMessageEncoding(MessageEncoding messageEncoding) {
        this.messageEncoding = messageEncoding;
    }

    /**
     * <p>Getter for the field <code>hl7Protocol</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.HL7Protocol} object.
     */
    public HL7Protocol getHl7Protocol() {
        return hl7Protocol;
    }

    /**
     * <p>Setter for the field <code>hl7Protocol</code>.</p>
     *
     * @param hl7Protocol a {@link net.ihe.gazelle.HL7Common.model.HL7Protocol} object.
     */
    public void setHl7Protocol(HL7Protocol hl7Protocol) {
        this.hl7Protocol = hl7Protocol;
    }

    /**
     * <p>Getter for the field <code>url</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUrl() {
        return url;
    }

    /**
     * <p>Setter for the field <code>url</code>.</p>
     *
     * @param uri a {@link java.lang.String} object.
     */
    public void setUrl(String uri) {
        this.url = uri;
    }

    /**
     * <p>getConfigurationsFiltered.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @param running        TODO
     * @param hl7Protocol    TODO
     * @return a {@link java.util.List} object.
     */
    public static List<SimulatorResponderConfiguration> getConfigurationsFiltered(String name, Actor simulatedActor,
                                                                                  Transaction transaction, Domain domain, EntityManager entityManager, Boolean running,
                                                                                  HL7Protocol hl7Protocol) {
        SimulatorResponderConfigurationQuery query = new SimulatorResponderConfigurationQuery(entityManager);

        if (name != null) {
            query.name().eq(name);
        }
        if (simulatedActor != null) {
            query.simulatedActor().eq(simulatedActor);
        }
        if (transaction != null) {
            query.transaction().eq(transaction);
        }
        if (domain != null) {
            query.domain().eq(domain);
        }
        if (running != null) {
            query.running().eq(running);
        }
        if (hl7Protocol != null) {
            query.hl7Protocol().eq(hl7Protocol);
        }
        query.name().order(true);
        return query.getListNullIfEmpty();
    }

    /**
     * <p>findSimulatorResponderConfigurationByActorAndDomain.</p>
     *
     * @param simulatedActorKeyword a {@link java.lang.String} object.
     * @param domainKeyword a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public static SimulatorResponderConfiguration findSimulatorResponderConfigurationByActorAndDomain(
            String simulatedActorKeyword, String domainKeyword) {
        SimulatorResponderConfigurationQuery query = new SimulatorResponderConfigurationQuery();
        query.simulatedActor().keyword().eq(simulatedActorKeyword);
        query.domain().keyword().eq(domainKeyword);
        return query.getUniqueResult();
    }

    /**
     * <p>findSimulatorResponderConfigurationByName.</p>
     *
     * @param serverName a {@link java.lang.String} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    @Transactional
    public static SimulatorResponderConfiguration findSimulatorResponderConfigurationByName(String serverName,
                                                                                            EntityManager entityManager) {
        SimulatorResponderConfigurationQuery query = new SimulatorResponderConfigurationQuery(entityManager);
        query.name().eq(serverName);
        return query.getUniqueResult();
    }

    /**
     * <p>getSimulatorResponderConfigurationById.</p>
     *
     * @param confId a {@link java.lang.Integer} object.
     * @return a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public static SimulatorResponderConfiguration getSimulatorResponderConfigurationById(Integer confId) {
        SimulatorResponderConfigurationQuery query = new SimulatorResponderConfigurationQuery();
        query.id().eq(confId);
        SimulatorResponderConfiguration resp = query.getUniqueResult();
        return resp;
    }

    /**
     * save the configuration
     *
     * @param entityManager TODO
     * @return a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     * @throws java.sql.BatchUpdateException if any.
     * @throws org.hibernate.exception.ConstraintViolationException if any.
     */
    @Transactional
    public SimulatorResponderConfiguration save(EntityManager entityManager) throws BatchUpdateException,
            ConstraintViolationException {
        SimulatorResponderConfiguration configuration = this;
        if (entityManager == null) {
            entityManager = EntityManagerService.provideEntityManager();
        }
        configuration = entityManager.merge(configuration);
        entityManager.flush();
        return configuration;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SimulatorResponderConfiguration other = (SimulatorResponderConfiguration) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}

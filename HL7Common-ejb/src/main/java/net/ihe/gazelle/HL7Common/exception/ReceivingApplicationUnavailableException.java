package net.ihe.gazelle.HL7Common.exception;

import ca.uhn.hl7v2.AcknowledgmentCode;

/**
 * <p>ReceivingApplicationUnavailableException class.</p>
 *
 * @author abe
 * @version 1.0: 16/04/19
 */

public class ReceivingApplicationUnavailableException extends HL7v2ParsingException {

    private static final String RECEIVING_APPLICATION_LOCATION = "MSH^1^5";

    public ReceivingApplicationUnavailableException(String recevingApplicationFromMessage) {
        super(recevingApplicationFromMessage, RECEIVING_APPLICATION_LOCATION);
    }

    @Override
    public String getErrorMessage(){
        return "Receiving application unavailable: " + getElementInError();
    }

    @Override
    public AcknowledgmentCode getAckCode() {
        return AcknowledgmentCode.AR;
    }
}

package net.ihe.gazelle.HL7Common.model;

import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Identity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by xfs on 25/04/16.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@Name("fhirResponderSUTConfiguration")
@DiscriminatorValue("fhir_responder")
public class FHIRResponderSUTConfiguration extends net.ihe.gazelle.simulator.sut.model.SystemConfiguration {

    private static final long serialVersionUID = 5093985945251904654L;
    /** Constant <code>_255="(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?"{trunked}</code> */
    public final static String _255 = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    /** Constant <code>_stringIPRegex_="^(?: + _255 + \\.){3} + _255 + $"</code> */
    public final static String _stringIPRegex_ = "^(?:" + _255 + "\\.){3}" + _255 + "$";
    /** Constant <code>PRIVATE_IP_REGEX="(^127\\.0\\.0\\.1)|(^10\\..*)|(^172\\.1"{trunked}</code> */
    public final static String PRIVATE_IP_REGEX = "(^127\\.0\\.0\\.1)|(^10\\..*)|(^172\\.1[6-9]\\..*)|(^172\\.2[0-9]\\..*)|(^172\\.3[0-1]\\..*)|(^192\\.168\\..*)";


    /**
     * <p>Constructor for FHIRResponderSUTConfiguration.</p>
     */
    public FHIRResponderSUTConfiguration() {
        super();
    }

    /**
     * <p>Constructor for FHIRResponderSUTConfiguration.</p>
     *
     * @param selectedSystemConfiguration a {@link net.ihe.gazelle.HL7Common.model.FHIRResponderSUTConfiguration} object.
     */
    public FHIRResponderSUTConfiguration(FHIRResponderSUTConfiguration selectedSystemConfiguration) {
        super(selectedSystemConfiguration);
        this.name = selectedSystemConfiguration.getName().concat("_COPY");
        this.url = selectedSystemConfiguration.getUrl();
    }


    /** {@inheritDoc} */
    @Override
    public String getEndpoint() {
        return super.getUrl();

    }


    /**
     * <p>getAllConfigurationsForSelection.</p>
     *
     * @param transactionKeyword a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public static List<FHIRResponderSUTConfiguration> getAllConfigurationsForSelection(String transactionKeyword) {
        FHIRResponderSUTConfigurationQuery query = new FHIRResponderSUTConfigurationQuery();
        query.listUsages().transaction().keyword().eq(transactionKeyword);
        query.name().order(true);
        if (!Identity.instance().isLoggedIn()) {
            query.isPublic().eq(true);
        } else if (!Identity.instance().hasRole("admin_role")) {

            GazelleIdentity gazelleIdentity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
            String institutionName = gazelleIdentity.getOrganisationKeyword();

            query.addRestriction(HQLRestrictions.or(query.ownerCompany().eqRestriction(institutionName), query.isPublic().eqRestriction(true)));
        }
        return query.getList();
    }
}


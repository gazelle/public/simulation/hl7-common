package net.ihe.gazelle.HL7Common.messages;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.v25.segment.EVN;
import ca.uhn.hl7v2.model.v25.segment.MSA;
import ca.uhn.hl7v2.model.v25.segment.MSH;
import ca.uhn.hl7v2.model.v25.segment.PV1;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Class description: <b>SegmentBuilder</b>
 * This class is aimed to gather all the methods to build common segments like MSH, EVN...
 * This class can be extended in each simulator in order to add specific segments
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes Bretagne Atlantique
 * @version 1.0 - 2011, July 18th
 */
public class SegmentBuilder {


    /**
     * Fill the given MSH segment
     *
     * @param mshSegment a {@link ca.uhn.hl7v2.model.v25.segment.MSH} object.
     * @param receivingApplication a {@link java.lang.String} object.
     * @param receivingFacility a {@link java.lang.String} object.
     * @param sendingApplication a {@link java.lang.String} object.
     * @param sendingFacility a {@link java.lang.String} object.
     * @param messageCode a {@link java.lang.String} object.
     * @param triggerEvent a {@link java.lang.String} object.
     * @param msgStructure a {@link java.lang.String} object.
     * @param hl7Version a {@link java.lang.String} object.
     * @param sdf a {@link java.text.SimpleDateFormat} object.
     * @param characterSet a {@link java.lang.String} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    public static void fillMSHSegment(MSH mshSegment,
                                      String receivingApplication, String receivingFacility,
                                      String sendingApplication, String sendingFacility,
                                      String messageCode, String triggerEvent,
                                      String msgStructure,
                                      String hl7Version,
                                      SimpleDateFormat sdf, String characterSet) throws DataTypeException {
        try {
            mshSegment.getFieldSeparator().setValue("|");
            mshSegment.getEncodingCharacters().setValue("^~\\&");
            mshSegment.getSendingApplication().getNamespaceID().setValue(sendingApplication);
            mshSegment.getSendingFacility().getNamespaceID().setValue(sendingFacility);
            mshSegment.getReceivingApplication().getNamespaceID().setValue(receivingApplication);
            mshSegment.getReceivingFacility().getNamespaceID().setValue(receivingFacility);
            mshSegment.getDateTimeOfMessage().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getMessageType().getMessageCode().setValue(messageCode);
            mshSegment.getMessageType().getTriggerEvent().setValue(triggerEvent);
            mshSegment.getMessageType().getMessageStructure().setValue(msgStructure);
            mshSegment.getProcessingID().getProcessingID().setValue("P");
            mshSegment.getMessageControlID().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getVersionID().getVersionID().setValue(hl7Version);
            if (characterSet != null) {
                mshSegment.getCharacterSet(0).setValue(characterSet);
            }
        } catch (DataTypeException e) {
            throw new DataTypeException("An error occurred when filling MSH segment: " + e.getMessage());
        }
    }

    /**
     * Fill the required fields of the given EVN segment
     *
     * @param evnSegment a {@link ca.uhn.hl7v2.model.v25.segment.EVN} object.
     * @param sdf a {@link java.text.SimpleDateFormat} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    public static void fillEVNSegment(EVN evnSegment, SimpleDateFormat sdf) throws DataTypeException {
        try {
            evnSegment.getRecordedDateTime().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));
        } catch (DataTypeException e) {
            throw new DataTypeException("An error occurred when filling EVN segment: " + e.getMessage());
        }
    }

    /**
     * Fill the required fields of the given PV1 segment
     *
     * @param pv1Segment a {@link ca.uhn.hl7v2.model.v25.segment.PV1} object.
     * @param patientClass a {@link java.lang.String} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    public static void fillPV1Segment(PV1 pv1Segment, String patientClass) throws DataTypeException {
        try {
            pv1Segment.getPatientClass().setValue(patientClass);
        } catch (DataTypeException e) {
            throw new DataTypeException("An error occurred when filling PV1 segment: " + e.getMessage());
        }
    }

    /**
     * <p>fillMSASegment.</p>
     *
     * @param msaSegment a {@link ca.uhn.hl7v2.model.v25.segment.MSA} object.
     * @param messageControlId a {@link java.lang.String} object.
     * @param ackCode a {@link java.lang.String} object.
     * @throws ca.uhn.hl7v2.model.DataTypeException if any.
     */
    public static void fillMSASegment(MSA msaSegment, String messageControlId, AcknowledgmentCode ackCode) throws DataTypeException {
        try {
            msaSegment.getAcknowledgmentCode().setValue(ackCode.name());
            msaSegment.getMessageControlID().setValue(messageControlId);
        } catch (DataTypeException e) {
            throw new DataTypeException("An error occurred when filling MSA segment: " + e.getMessage());
        }
    }

}

package net.ihe.gazelle.HL7Common.model;

/**
 * <b>Class Description : </b>ValidatorType<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 08/09/15
 */
public enum ValidatorType {
    V2("HL7v2"),
    V3("HL7v3"),
    FHIR("FHIR");
    String label;
    /**
     * <p>Constructor for ValidatorType.</p>
     */
    ValidatorType(String label){
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}

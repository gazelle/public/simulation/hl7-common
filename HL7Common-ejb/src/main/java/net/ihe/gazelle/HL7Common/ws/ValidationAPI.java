package net.ihe.gazelle.HL7Common.ws;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;


@Local
@Path("/hl7Messages")
public interface ValidationAPI {

    @GET
    @Path("/launchValidation")
    @Produces("text/plain")
    javax.ws.rs.core.Response launchValidation(@QueryParam("messageId") String messageID,
                                               @QueryParam("issuer") String issuer);


    @GET
    @Path("/validationStatus")
    @Produces("text/plain")
    javax.ws.rs.core.Response validationStatus(@QueryParam("messageId") String messageID,
                                               @QueryParam("issuer") String issuer);

    @GET
    @Path("/validationReport")
    @Produces("application/xml")
    javax.ws.rs.core.Response validationReport(@QueryParam("messageId") String messageID,
                                               @QueryParam("issuer") String issuer);


}
package net.ihe.gazelle.HL7Common.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Class description : <b>Charset</b>
 * This class describes the charset used to send and received HL7 messages
 * <p/>
 * This class contains the following attributes:
 * <ul>
 * <li><b>Id</b> Id in the database</li>
 * <li><b>displayName</b> The name which is displayed to the user. This attribute is required to be unique eg. UTF-8</li>
 * <li><b>hl7Code</b> The HL7 code used to fill the MSH-18 field of the message. This code comes from the HL7 table 0211 eg. UNICODE UTF-8</li>
 * <li><b>javaCode</b> The code required by InputStreamReader constructor to set the charset of the reader eg. UTF8</li>
 * </ul>
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes - Bretagne Atlantique
 * @version 1.0 - 2011, July 8th
 */

@Entity
@Name("charset")
@Table(name = "hl7_charset", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "display_name"))
@SequenceGenerator(name = "hl7_charset_sequence", sequenceName = "hl7_charset_id_seq", allocationSize = 1)
public class Charset implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8787641567800190784L;

    @Id
    @NotNull
    @GeneratedValue(generator = "hl7_charset_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @NotNull
    @Column(name = "display_name")
    private String displayName;

    @Column(name = "hl7_code")
    private String hl7Code;

    @Column(name = "java_code")
    private String javaCode;

    /**
     * <p>Constructor for Charset.</p>
     */
    public Charset() {

    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>displayName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * <p>Setter for the field <code>displayName</code>.</p>
     *
     * @param displayName a {@link java.lang.String} object.
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * <p>Getter for the field <code>hl7Code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getHl7Code() {
        return hl7Code;
    }

    /**
     * <p>Setter for the field <code>hl7Code</code>.</p>
     *
     * @param hl7Code a {@link java.lang.String} object.
     */
    public void setHl7Code(String hl7Code) {
        this.hl7Code = hl7Code;
    }

    /**
     * <p>Getter for the field <code>javaCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getJavaCode() {
        return javaCode;
    }

    /**
     * <p>Setter for the field <code>javaCode</code>.</p>
     *
     * @param javaCode a {@link java.lang.String} object.
     */
    public void setJavaCode(String javaCode) {
        this.javaCode = javaCode;
    }

    /**
     * <p>getAllCharsets.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public static List<Charset> getAllCharsets() {
        CharsetQuery query = new CharsetQuery();
        query.displayName().order(true);
        return query.getListNullIfEmpty();
    }

    /**
     * <p>getCharsetByHL7Name.</p>
     *
     * @param hl7Name a {@link java.lang.String} object.
     * @return a {@link net.ihe.gazelle.HL7Common.model.Charset} object.
     */
    public static Charset getCharsetByHL7Name(String hl7Name) {
        CharsetQuery query = new CharsetQuery();
        query.hl7Code().eq(hl7Name);
        return query.getUniqueResult();
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((displayName == null) ? 0 : displayName.hashCode());
        result = prime * result + ((hl7Code == null) ? 0 : hl7Code.hashCode());
        result = prime * result
                + ((javaCode == null) ? 0 : javaCode.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Charset other = (Charset) obj;
        if (displayName == null) {
            if (other.displayName != null)
                return false;
        } else if (!displayName.equals(other.displayName))
            return false;
        if (hl7Code == null) {
            if (other.hl7Code != null)
                return false;
        } else if (!hl7Code.equals(other.hl7Code))
            return false;
        if (javaCode == null) {
            if (other.javaCode != null)
                return false;
        } else if (!javaCode.equals(other.javaCode))
            return false;
        return true;
    }


}
